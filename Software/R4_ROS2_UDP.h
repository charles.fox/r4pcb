// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 ROS2 WIFI UDP

#include <Arduino.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include "R4_Def.h"

#ifndef R4_ROS2_UDP_H
  #define R4_ROS2_UDP_H

  //#define SSID            "Xiaomi 12"
  //#define SSID_PW         "1234567890"

  #define SSID            "Chris"
  #define SSID_PW         "abcd1234"

  #define UDP_REMOTE_PORT  2390
  #define GPIO43LEDpin    43    // Pin 43 control on-board LED_GREEN on the R4 board

  void   R4_ROS2_UDP_Init();
  String R4_ROS2_UDP_Client();
  void   R4_ROS2_UDP_Connect();
  bool   R4_ROS2_SendUDPPacket(String packet);
  bool   R4_ROS2_checkPacketCRC(String checkPacketincoming, String packetCRCincoming);
#endif