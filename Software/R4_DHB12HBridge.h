// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// DHB12 4 Channel H-Bridge Controller via PCA96895 PWM IC on the R4 Board

#include <Arduino.h>

//https://www.arduino.cc/reference/en/libraries/adafruit-pwm-servo-driver-library/
//Version 2.4.1
#include <Adafruit_PWMServoDriver.h> //Version 2.4.1

#include "R4_Def.h"

#ifndef ADAFRUIT_PWMSERVODRIVER_H
 #define ADAFRUIT_PWMSERVODRIVER_H
#endif

#ifndef R4_DHB12HBRIDGE_H
  #define   R4_DHB12HBRIDGE_H

  #define   R4_DHB12HBridge_device_Address 0x41

  #define	  DHB12_1_A_EN   5  //(DHB12-1	10) (LED5 in the datasheet not the IC pin number! Pin 11 on IC)
  #define	  DHB12_1_A_IN1  1	//(DHB12-1	 6) (LED1 in the datasheet not the IC pin number! Pin  7 on IC)
  #define	  DHB12_1_A_IN2  3 	//(DHB12-1	 8) (LED3 in the datasheet not the IC pin number! Pin  9 on IC)

  #define 	DHB12_1_B_EN   4 	//(DHB12-1	 9) (LED4 in the datasheet not the IC pin number! Pin 10 on IC)
  #define		DHB12_1_B_IN1  0	//(DHB12-1	 5) (LED0 in the datasheet not the IC pin number! Pin  6 on IC)
  #define		DHB12_1_B_IN2  2	//(DHB12-1	 7) (LED2 in the datasheet not the IC pin number! Pin  8 on IC)

  #define		DHB12_2_A_EN  11 	//(DHB12-2	10) (LED11 in the datasheet not the IC pin number! Pin 18 on IC)
  #define	  DHB12_2_A_IN1	 7  //(DHB12-2	 6) (LED 7 in the datasheet not the IC pin number! Pin 13 on IC)
  #define	  DHB12_2_A_IN2	 9  //(DHB12-2	 8) (LED 9 in the datasheet not the IC pin number! Pin 16 on IC)

  #define	  DHB12_2_B_EN	10  //(DHB12-2	 9) (LED10 in the datasheet not the IC pin number! Pin 17 on IC)
  #define	  DHB12_2_B_IN1	 6  //(DHB12-2	 5) (LED 6 in the datasheet not the IC pin number! Pin 12 on IC)
  #define	  DHB12_2_B_IN2	 8  //(DHB12-2	 7) (LED 8 in the datasheet not the IC pin number! Pin 15 on IC)

  // The OSMC Disable Pins share this chip - define the pins and provide the disable functions with this library

  #define	  OSMC1_DIS  	  12  //(OSMC1 	 4) (LED12 in the datasheet not the IC pin number! Pin 19 on IC)
  #define	  OSMC2_DIS  	  21  //(OSMC2	 4) (LED13 in the datasheet not the IC pin number! Pin 21 on IC)
  #define	  OSMC3_DIS  	  20  //(OSMC3	 4) (LED14 in the datasheet not the IC pin number! Pin 20 on IC)
  #define	  OSMC4_DIS  	  22  //(OSMC4	 4) (LED15 in the datasheet not the IC pin number! Pin 22 on IC)

  // ********** INTERFACE *********************************************************************************************************

  void R4_DHB12_Init();
  void R4_TestDHB12();
  void R4_DHB12_DisableAll();
  
  void R4_DHB12_1_A_Enable();
  void R4_DHB12_1_A_Disable();
  void R4_DHB12_1_B_Enable();
  void R4_DHB12_1_B_Disable();
  void R4_DHB12_2_A_Enable();
  void R4_DHB12_2_A_Disable();
  void R4_DHB12_2_B_Enable();
  void R4_DHB12_2_B_Disable();

  void R4_DHB12_1_A_setSpeedandDirection(uint16_t speed, bool direction);
  void R4_DHB12_1_B_setSpeedandDirection(uint16_t speed, bool direction);
  void R4_DHB12_2_A_setSpeedandDirection(uint16_t speed, bool direction);
  void R4_DHB12_2_B_setSpeedandDirection(uint16_t speed, bool direction);
  
  speed_direction_type R4_DHB12_getSpeedandDirection(int channel);
  void R4_DHB12_setSpeedandDirection(uint8_t R4_DHB12IN1_channel, uint8_t R4_DHB12IN2_channel, uint16_t speed, bool direction);
  

  //void brake()
#endif

