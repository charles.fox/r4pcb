// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 16 Channel Servo Bank controller via PCA96895 PWM IC on the R4 Board

#include <Arduino.h>

//https://www.arduino.cc/reference/en/libraries/adafruit-pwm-servo-driver-library/
//Version 2.4.1
#include <Adafruit_PWMServoDriver.h>  //Version 2.4.1

#ifndef ADAFRUIT_PWMSERVODRIVER_H
 #define ADAFRUIT_PWMSERVODRIVER_H
#endif

#ifndef R4SERVOBANK_H
 #define R4SERVOBANK_H
 #define R4_ServoBank_device_Address 0x42
#endif

#ifndef R4_ServoBank_h
  #define R4_ServoBank_h
  #include "Arduino.h" 
  
  void R4_ServoBank_Init();
  void R4_ServoBank_test();
  void R4_ServoBank_disableAll();
  void R4_ServoBank_setChannel_ServoPulseDuration(uint16_t Microseconds, uint8_t Channel);
  void R4_ServoBank_setChannel_Ontime_OffTime(uint8_t Channel, uint16_t on_Microseconds, uint16_t off_Microseconds);
  uint8_t R4_ServoBank_getChannel_ServoPulseDuration(uint8_t Channel);
  
#endif
