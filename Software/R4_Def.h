// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 Defines datatypes and pins 

#define GPIO46LEDpin       46          // Pin 46 control on-board LED_GREEN on the R4 board
#define GPIO43LEDpin       43          // Pin 43 control on-board LED_GREEN on the R4 board
#define DMHRelay_GPIOpin   40

#ifndef R4_TYPES_H
 #define R4_TYPES_H
  #define DEBUGSerial  // if defined serial debug data from Serial0
  //#define DEBUGTFT   // if defined serial debug data from Serial1 to Due TFT

  struct speed_direction_type
  {
    int   Duration  = 0; 
    bool  Direction = 0;
  };

  struct dhb12_type
  {
    int   Duration__1A = 0; 
    bool  Direction_1A = 0;
    int   Duration__1B = 0;
    bool  Direction_1B = 0;
    int   Duration__2A = 0;
    bool  Direction_2A = 0;
    int   Duration__2B = 0;
    bool  Direction_2B = 0;
  };

  struct osmc_type
  {
    int   Duration__1 = 0; 
    bool  Direction_1 = 0;
    int   Duration__2 = 0;
    bool  Direction_2 = 0;
    int   Duration__3 = 0;
    bool  Direction_3 = 0;
    int   Duration__4 = 0;
    bool  Direction_4 = 0;
  };

  struct podcar_data_type
  {
    double Reading_24V   = 0;
    double Reading_12V   = 0;                                      
    double Reading__5V   = 0;
    double Reading_DMH   = 0;
    double Steer_FBack   = 0;
    bool   DMHRelayState = 0;
    speed_direction_type dhb12_1A;
    speed_direction_type osmc_1;
  };

#endif

/*
Device	Device PIN	Function	Connection	Pin																	
																					
XA1	A6	A00	24V Fused 	J1																	
XA1	A7	A01	12V Fused	J2																	
XA1	A8	A02	5V Fused	J3																	
XA1	A9	A03	DMH_IN	J4																	
XA1	A10	A04																			
XA1	A11	A05																			
																					
U7	28	BLDC1_BRK	BLDC1																		
U7	27	BLDC1_DIR	BLDC1																		
U8	9	BLDC_DACA	BLDC1																		
U7	26	BLDC2_BRK	BLDC2																		
U7	25	BLDC2_DIR	BLDC2																		
U8	8	BLDC_DACB	BLDC2																		
U7	24	BLDC3_BRK	BLDC3																		
U7	23	BLDC3_DIR	BLDC3																		
U8	6	BLDC_DACC	BLDC3																		
U7	22	BLDC4_BRK	BLDC4																		
U7	21	BLDC4_DIR	BLDC4																		
U8	7	BLDC_DACD	BLDC4																		
XA1	D52	BLDC_LDAC	ALLBLDC																		
XA1	D53	BLDC_RDY{slash}BSY	ALLBLDC																		
																					
U4	11	DBH12_1_A_EN	DHB12-1	10																	
U4	7	DBH12_1_A_IN1	DHB12-1	6																	
U4	9	DBH12_1_A_IN2	DHB12-1	8																	
U4	10	DBH12_1_B_EN	DHB12-1	9																	
U4	6	DBH12_1_B_IN1	DHB12-1	5																	
U4	8	DBH12_1_B_IN2	DHB12-1	7																	
U4	18	DBH12_2_A_EN	DHB12-2	10																	
U4	13	DBH12_2_A_IN1	DHB12-2	6																	
U4	16	DBH12_2_A_IN2	DHB12-2	8																	
U4	17	DBH12_2_B_EN	DHB12-2	9																	
U4	12	DBH12_2_B_IN1	DHB12-2	5																	
U4	15	DBH12_2_B_IN2	DHB12-2	7																	
																					
XA1	D50	DIR-(DIR)_1	Stepper_1	3									
XA1	D51	PUL-(PUL)_1	Stepper_1	5								
XA1	D49	ENA-(ENA)	Stepper_1	1																	
XA1	D47	DIR-(DIR)_2	Stepper_2	3																	
XA1	D48	PUL-(PUL)_2	Stepper_2	5																	
XA2	D49	ENA-(ENA)	Stepper_2	1																	
XA1	D44	DIR-(DIR)_3	Stepper_3	3																
XA1	D45	PUL-(PUL)_3	Stepper_3	5																	
XA3	D49	ENA-(ENA)	Stepper_3	1																
XA1	D41	DIR-(DIR)_4	Stepper_4	3																	
XA1	D42	PUL-(PUL)_4	Stepper_4	5																
XA4	D49	ENA-(ENA)	Stepper_4	1																	
														
XA1	D38	ENC1_A	ENC1	4																	
XA1	D39	ENC1_B	ENC1	5																
XA1	D23	ENC1_S	ENC1	3																	
XA1	D36	ENC2_A	ENC2	4																
XA1	D37	ENC2_B	ENC2	5																	
XA1	D22	ENC2_S	ENC2	3																	
XA1	D34	ENC3_A	ENC3	4																
XA1	D35	ENC3_B	ENC3	5																
XA1	D14	ENC3_S	ENC3	3																
XA1	D32	ENC4_A	ENC4	4																
XA1	D33	ENC4_B	ENC4	5																	
XA1	D15	ENC4_S	ENC4	3																	
																					
XA1	D30	ULT1_ECHO	HC-SR1	2																	
XA1	D31	ULT1_TRIG	HC-SR1	3																	
XA1	D28	ULT2_ECHO	HC-SR2	2																	
XA1	D29	ULT2_TRIG	HC-SR2	3																	
XA1	D26	ULT3_ECHO	HC-SR3	2																	
XA1	D27	ULT3_TRIG	HC-SR3	3																	
XA1	D24	ULT4_ECHO	HC-SR4	2																	
XA1	D25	ULT4_TRIG	HC-SR4	3			

XA1	D40	DMHRelay_GPIOPin	DMH_RLY1	3																	
																					
U3	15	OSMC1_AHI	OSMC1	5																	
U3	16	OSMC1_ALI	OSMC1	6																	
U3	18	OSMC1_BHI	OSMC1	7																	
U3	17	OSMC1_BLI	OSMC1	8																	
U4	19	OSMC1_DIS	OSMC1	4																	
U3	6	  OSMC2_AHI	OSMC3	5																	
U3	7	  OSMC2_ALI	OSMC3	6																	
U3	9	  OSMC2_BHI	OSMC3	7																	
U3	8	  OSMC2_BLI	OSMC3	8																	
U4	21	OSMC2_DIS	OSMC3	4																	
U3	19	OSMC3_AHI	OSMC2	5																	
U3	20	OSMC3_ALI	OSMC2	6																	
U3	22	OSMC3_BHI	OSMC2	7																	
U3	21	OSMC3_BLI	OSMC2	8																	
U4	20	OSMC3_DIS	OSMC2	4																	
U3	10	OSMC4_AHI	OSMC4	5																	
U3	11	OSMC4_ALI	OSMC4	6																	
U3	13	OSMC4_BHI	OSMC4	7																	
U3	12	OSMC4_BLI	OSMC4	8																	
U4	22	OSMC4_DIS	OSMC4	4																	
																					
U6	6	  S1_PWM	S1	2																	
U6	7	  S2_PWM	S2	2																	
U6	8	  S3_PWM	S3	2																	
U6	9	  S4_PWM	S4	2																	
U6	10	S5_PWM	S5	2																	
U6	11	S6_PWM	S6	2																	
U6	12	S7_PWM	S7	2																	
U6	13	S8_PWM	S8	2																	
U6	15	S9_PWM	S9	2																	
U6	16	S10_PWM	S10	2																	
U6	17	S11_PWM	S11	2																	
U6	18	S12_PWM	S12	2																	
U6	19	S13_PWM	S13	2																	
U6	20	S14_PWM	S14	2																	
U6	21	S15_PWM	S15	2																	
U6	22	S16_PWM	S16	2																	
																					
U7	1	IN8_5V	RelayPort_1	9																	
U7	2	IN7_5V	RelayPort_1	8																	
U7	3	IN6_5V	RelayPort_1	7																	
U7	4	IN5_5V	RelayPort_1	6																	
U7	5	IN4_5V	RelayPort_1	5																	
U7	6	IN3_5V	RelayPort_1	4																	
U7	7	IN2_5V	RelayPort_1	3																	
U7	8	IN1_5V	RelayPort_1	2																	
																					
XA1	D21	SCL	U3	26		U4		U6	26	U7	12				U8	2	LCD_I2C1	4	R6	2	
XA1	D20	SDA	U3	27		U4		U6	27	U7	13				U8	3	LCD_I2C1	3	R5	2	
																					
XA1	SCL1	SCL1	3.3VEXT_I2C1	3		R3			2												
XA1	SDA1	SDA1	3.3VEXT_I2C1	2		R4			2												
																					
XA1	D18	TX1	UART1	2																	
XA1	D19	RX1	UART1	3																	
																					
XA1	D16	TX2	UART2	2																	
XA1	D17	RX2	UART2	3																	
																					
XA1	D46	GPIO_LED46																			
XA1	D43	GPIO_SWITCH43																			
																					
XA1	CANR	CAN_RX	U5	4																	
XA1	CANT	CAN_TX	U5	1																	
																					
XA1	DAC0	DAC_0	DAC_1	3																	
XA1	DAC1	DAC_1	DAC_1	2																	

*/