// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// OSMC 4 Channel H-Bridge Controller via PCA96895 PWM IC on the R4 Board

#include <Arduino.h>
//https://www.arduino.cc/reference/en/libraries/adafruit-pwm-servo-driver-library/
#include <Adafruit_PWMServoDriver.h>  //Version 2.4.1

#include "R4_Def.h"

#ifndef ADAFRUIT_PWMSERVODRIVER_H
 #define ADAFRUIT_PWMSERVODRIVER_H
#endif

#ifndef R4_OSMCHBRIDGE_H
  #define   R4_OSMCHBRIDGE_H

  #define   R4_OSMCHBridge_device_Address    0x40
  #define   R4_OSMCHBridge_EN_device_Address 0x41

  #define	OSMC1_ALI	9
  #define OSMC1_AHI	8														
	#define	OSMC1_BLI	10									
  #define	OSMC1_BHI	11																
  
  #define	OSMC2_AHI	 12															
  #define	OSMC2_ALI	 13																
  #define	OSMC2_BHI	 15														
  #define	OSMC2_BLI  14 

  #define	OSMC3_AHI	0															
  #define	OSMC3_ALI	1															
  #define	OSMC3_BHI	3															
  #define	OSMC3_BLI 2

  #define	OSMC4_AHI	4															
  #define	OSMC4_ALI	5															
  #define	OSMC4_BHI	7																
  #define	OSMC4_BLI	6

  // The OSMC Disable Pins are provided by the DBH12 - the disable functions s library

  #define	OSMC1_DIS 12  //(OSMC1 	 4) (LED12 in the datasheet not the IC pin number! Pin 19 on IC)
  #define	OSMC2_DIS 21  //(OSMC2	 4) (LED13 in the datasheet not the IC pin number! Pin 21 on IC)
  #define	OSMC3_DIS 20  //(OSMC3	 4) (LED14 in the datasheet not the IC pin number! Pin 20 on IC)
  #define	OSMC4_DIS 22  //(OSMC4	 4) (LED15 in the datasheet not the IC pin number! Pin 22 on IC)

  // ********** INTERFACE *********************************************************************************************************

  void R4_OSMC_EN_DIS_Init();
  void R4_OSMC_Init();
  
  void R4_TestOSMC1();
  void R4_TestOSMC2();
  void R4_TestOSMC3();
  void R4_TestOSMC4();

  void R4_OSMC_DisableAll();
  void R4_OSMC1_Enable();
  void R4_OSMC1_Disable();
  void R4_OSMC2_Enable();
  void R4_OSMC2_Disable();
  void R4_OSMC3_Enable();
  void R4_OSMC3_Disable();
  void R4_OSMC4_Enable();
  void R4_OSMC4_Disable();

  void R4_OSMC1_setSpeedandDirection(uint16_t speed, bool direction);
  void R4_OSMC2_setSpeedandDirection(uint16_t speed, bool direction);
  void R4_OSMC3_setSpeedandDirection(uint16_t speed, bool direction);
  void R4_OSMC4_setSpeedandDirection(uint16_t speed, bool direction); 

  speed_direction_type R4_OSMC_getSpeedandDirection(int channel);
  void R4_OSMC_setSpeedandDirection(uint8_t R4_OSMC_AHI_channel, uint8_t R4_OSMC_ALI_channel, uint8_t R4_OSMC_BHI_channel, uint8_t R4_OSMC_BLI_channel, uint16_t speed, bool direction);
#endif
