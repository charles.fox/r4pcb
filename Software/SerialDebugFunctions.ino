// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// Put your Serial debug statements here. 
// Use #define DEBUGSerial in R4_Def.h to switch this and other DEBUG output on.

void SerialDebug()
{
      // Output the analog value via USB to the Serial Terminal  
      String stringAIN1Voltage = String(podcar_data.Reading_24V ,2);
      //Serial.print("24V_Reading:");
      //Serial.println(stringAIN1Voltage); 

    // Output the analog value via USB to the Serial Terminal  
      String stringAIN2Voltage = String(podcar_data.Reading_12V,2);
      //Serial.print("12V_Reading:");
      //Serial.println(stringAIN2Voltage);   

    // Output the analog value via USB to the Serial Terminal  
      String stringAIN3Voltage = String(podcar_data.Reading__5V,2);
      //Serial.print("5V__Reading:");
      //Serial.println(stringAIN3Voltage); 

    // Output the analog value via USB to the Serial Terminal  
      String stringAIN4Voltage = String(podcar_data.Reading_DMH,2);
      //Serial.print("DMH_Reading:");
      //Serial.println(stringAIN4Voltage); 

    // Output the analog value via USB to the Serial Terminal  
      String stringAIN5Voltage = String(podcar_data.Steer_FBack,2);
      //Serial.print("Steer_FBack:");
      //Serial.println(stringAIN5Voltage); 

    // Output the DHB121A state via USB to the Serial Terminal  
      String stringDHB121A_Duration  = String(podcar_data.dhb12_1A.Duration);
      String stringDHB121A_Direction = String(podcar_data.dhb12_1A.Direction);
      Serial.print("DHB121A_Direction_Speed:");
      Serial.println(stringDHB121A_Duration + "," + stringDHB121A_Direction); 

    // Output the OSMC1 state via USB to the Serial Terminal  
      String stringOSMC1_Duration  = String(podcar_data.osmc_1.Duration);
      String stringOSMC1_Direction = String(podcar_data.osmc_1.Direction);
      Serial.print("OSMC1_Direction_Speed:");
      Serial.println(stringOSMC1_Duration + "," + stringOSMC1_Direction); 

      Serial.print("Loop Time  :");
      Serial.print(lt); 
      Serial.println(" mS");    
      Serial.print("Average Heart Beat Interval: ");
      Serial.println(lastaverage);
      Serial.print("haltState = ");
      Serial.println(haltState);
      Serial.print("stopCondition = ");
      Serial.println(stopCondition);
      Serial.print("loopcounter = ");
      Serial.println(loopcounter);
      Serial.print("servo_Duration = ");
      Serial.println(servo_Duration);
}

/*
void SerialTFT()
{
      Serial2.print("N:"); //NewLine Command after all values transmitted
      Serial2.println("Newline"); //NewLine Command after all values transmitted

    // Output the analog value via UART2 to the Serial TFT  
      String stringAIN2Voltage = String(AIN2Voltage,2);
      Serial2.print("A:");
      Serial2.print(stringAIN2Voltage);   
      Serial2.println(" Volts");

    // Output the latest DAC value via UART2 to the Serial TFT  
      String string_R4_4CH_DAC_currentVoltage = String(R4_4CH_DAC_currentVoltage,2);
      Serial2.print("D:");
      Serial2.print(string_R4_4CH_DAC_currentVoltage);
      Serial2.println(" Volts");

      //String ipAdd = R4_IpAddress2String(ip);
      // Output the latest DAC via uart2 to the DUE debug screen
      //Serial2.print("I:");  
      //Serial2.println(ipAdd);

      // Output the latest Wifi Status via uart2 to the DUE debug screen
      //String stats = WiFi.status();
      //Serial2.print("S:");
      //Serial2.println(stats);

      //Serial2.print("T:");
      //Serial2.println(lt);  
      
      Serial2.print("R:");
      Serial2.println(incomingUDPbuffer); 
}
*/