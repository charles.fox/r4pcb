// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 Board Analog channel read functions

#include "R4_Analog.h"

double AIN24_Calibrate        = 0;
double AIN12_Calibrate        = 0;
double  AIN5_Calibrate        = 0;
double  DMH_Calibrate         = 0;
double  ST_FeedBack_Calibrate = 0;
double UNUSED_AIN6_Calibrate  = 0;

R4_Analog::R4_Analog() 
{ 
 // Assign the Calibration value for each voltage input
 // These values depend on the pot position for each channel and the input voltage
 // Use a multimeter to measure the voltages and calculate the required calibration value (n mv per division)

 AIN24_Calibrate       = 25.9;
 AIN12_Calibrate       = 14.4;
 AIN5_Calibrate        =  7.0;
 DMH_Calibrate         = 14.4;
 ST_FeedBack_Calibrate = 13.9;
 UNUSED_AIN6_Calibrate = 10.0;
}
 
double R4_Analog::ReadAIN1()
{
  // read the AIN1 analog pin
  int val = analogRead(AIN1pin);        
  double voltage = ((double)val/1024) * AIN24_Calibrate;           // Calculate the Voltage 
  return voltage;
}

double R4_Analog::ReadAIN2()
{
  // read the AIN2 analog pin
  int val = analogRead(AIN2pin);        
  double voltage = ((double)val/1024) * AIN12_Calibrate;           // Calculate the Voltage 
  return voltage;
}

double R4_Analog::ReadAIN3()
{
  // read the AIN3 analog pin
  int val = analogRead(AIN3pin);        
  double voltage = ((double)val/1024) * AIN5_Calibrate;             // Calculate the Voltage 
  return voltage;
}

double R4_Analog::ReadAIN4()
{
  // read the AIN4 analog pin
  int val = analogRead(AIN4pin);        
  double voltage = ((double)val/1024) * DMH_Calibrate;           // Calculate the Voltage 
  return voltage;
}      

double R4_Analog::ReadAIN5()
{
  // read the AIN5 analog pin
  int val = analogRead(AIN5pin);        
  double voltage = ((double)val/1024) * ST_FeedBack_Calibrate;           // Calculate the Voltage 
  return voltage;
}      

double R4_Analog::ReadAIN6()
{
  // read the AIN6 analog pin
  int val = analogRead(AIN6pin);        
  double voltage = ((double)val/1024) * UNUSED_AIN6_Calibrate;           // Calculate the Voltage 
  return voltage;
}   
