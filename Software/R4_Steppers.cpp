// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// Stepper Functions for the R4 Board

#ifndef ACCELSTEPPER_H
 //https://www.arduino.cc/reference/en/libraries/accelstepper/
 #include <AccelStepper.h>
#endif

#include "R4_Steppers.h"

// Define some steppers and the pins they will use
AccelStepper Stepper1 (AccelStepper::DRIVER, Stepper1StepPin, Stepper1DirPin);    //X Shield Socket
AccelStepper Stepper2 (AccelStepper::DRIVER, Stepper2StepPin, Stepper2DirPin);    //Y Shield Socket
AccelStepper Stepper3 (AccelStepper::DRIVER, Stepper3StepPin, Stepper3DirPin);    //Z Shield Socket
AccelStepper Stepper4 (AccelStepper::DRIVER, Stepper4StepPin, Stepper4DirPin);    //A Shield Socket

int maxspeedHoming =   5000;
int accHoming      =    150;
int maxspeed       =   5000;
int acc            =    150;

// Steering Limit Pin Related
bool Stepper1LimitPinFlag      = 0;
bool Stepper2LimitPinFlag      = 0;
bool Stepper3LimitPinFlag      = 0;
bool Stepper4LimitPinFlag      = 0;

// Steering positioning and homing - Front has Blue Gearbox Arms
int commandPos                 =    0;
int homingDistance             =  100;  // + 100 to -100 steps = 200 steps (full stepping setting on driver)
int Stepper1CentrePos          = 1000;     
int Stepper2CentrePos          = 1000;     
int Stepper3CentrePos          = 1000;    
int Stepper4CentrePos          = 1700;    

long dellay = 0;                       // loop time delay counter variable

void R4_SteppersInit()
{
  Stepper1.setMaxSpeed(maxspeed);
  Stepper1.setAcceleration(acc); 
  Stepper1.moveTo(homingDistance);
  
  Stepper2.setMaxSpeed(maxspeed);
  Stepper2.setAcceleration(acc);
  Stepper2.moveTo(homingDistance);
  
  Stepper3.setMaxSpeed(maxspeed);
  Stepper3.setAcceleration(acc);
  Stepper3.moveTo(homingDistance); 

  Stepper4.setMaxSpeed(maxspeed);
  Stepper4.setAcceleration(acc);
  Stepper4.moveTo(homingDistance); 
}

void R4_StartSteppers()
{
  Stepper1.moveTo(homingDistance);
  Stepper2.moveTo(homingDistance);
  Stepper3.moveTo(homingDistance); 
  Stepper4.moveTo(homingDistance); 
}

void R4_StopSteppers()
{
  Stepper1.stop();
  Stepper2.stop();
  Stepper3.stop(); 
  Stepper4.stop();  
}

long R4_Stepper_getPosition(int channel)
{
  long returnPosition = 0;

  switch(channel)
  {
    case 1: 
      returnPosition = Stepper1.currentPosition();
    break;
    case 2: 
      returnPosition = Stepper2.currentPosition();
    break;
    case 3: 
      returnPosition = Stepper3.currentPosition();
    break;
    case 4: 
      returnPosition = Stepper4.currentPosition();
    break;
    default:
      returnPosition = -1;
    break;
  }

  return returnPosition;
}

void R4_BounceSteppers()
{
  // If at the end of travel go to the other end
  if (Stepper1.distanceToGo() == 0)
    Stepper1.moveTo(-Stepper1.currentPosition());
  if (Stepper2.distanceToGo() == 0)
    Stepper2.moveTo(-Stepper2.currentPosition());
  if (Stepper3.distanceToGo() == 0)
    Stepper3.moveTo(-Stepper3.currentPosition());
  if (Stepper4.distanceToGo() == 0)
    Stepper4.moveTo(-Stepper4.currentPosition());
}

void R4_updateLimits()
{ 
  Stepper1LimitPinFlag  = !digitalRead (Stepper1LimitPin);           // the limit pins use inverted logic - the pin being high means the switch is not pressed, so the input is inverted when reading the pin 
  Stepper2LimitPinFlag  = !digitalRead (Stepper2LimitPin);   // so that code reads as though the pin is high when the limit switch is pressed, this eases physically wiring the switches to the MEGA 
  Stepper3LimitPinFlag  = !digitalRead (Stepper3LimitPin);
  Stepper4LimitPinFlag  = !digitalRead (Stepper4LimitPin);
}

void R4_displayLimits()
{
  R4_updateLimits();
  Serial.print  (" FR:");
  Serial.print  (Stepper1LimitPinFlag);
  Serial.print  (" FL:");
  Serial.print  (Stepper2LimitPinFlag);
  Serial.print  (" RR:");
  Serial.print  (Stepper3LimitPinFlag);
  Serial.print  (" RL:");
  Serial.println(Stepper4LimitPinFlag);
}

void R4_displayDisToGo()
{
  Serial.print  (" FR:");
  Serial.print  (Stepper1.distanceToGo());
  Serial.print  (" FL:");
  Serial.print  (Stepper2.distanceToGo());
  Serial.print  (" RR:");
  Serial.print  (Stepper3.distanceToGo());
  Serial.print  (" RL:");
  Serial.println(Stepper4.distanceToGo());
}

void R4_displaySteeringState()
{ 
  R4_updateLimits();
  if(dellay++>5000)  //5000 loops between each output.
  {
    Serial.println("Steering State");
    Serial.print("FR Distance to go: ");
    Serial.print(Stepper1.distanceToGo());
    Serial.print(" Current Position: ");
    Serial.print(Stepper1.currentPosition());
    Serial.print(" FR Limit: ");
    Serial.println(Stepper1LimitPinFlag);

    Serial.print("FL Distance to go: ");
    Serial.print(Stepper2.distanceToGo());
    Serial.print(" Current Position: ");
    Serial.print(Stepper2.currentPosition());
    Serial.print(" FL Limit: ");
    Serial.println(Stepper2LimitPinFlag);

    Serial.print("RR Distance to go: ");
    Serial.print(Stepper3.distanceToGo());
    Serial.print(" Current Position: ");
    Serial.print(Stepper3.currentPosition());
    Serial.print(" RR Limit: ");
    Serial.println(Stepper3LimitPinFlag);

    Serial.print("RL Distance to go: ");
    Serial.print(Stepper4.distanceToGo());
    Serial.print(" Current Position: ");
    Serial.print(Stepper4.currentPosition());
    Serial.print(" RL Limit: ");
    Serial.println(Stepper4LimitPinFlag);
    Serial.println("");
    dellay = 0;
  }  
}

void R4_displaySteeringStateNow()
{ 
     Serial.println("Steering State");
     Serial.print("FR Distance to go: ");
     Serial.print(Stepper1.distanceToGo());
     Serial.print(" Current Position: ");
     Serial.print(Stepper1.currentPosition());
     Serial.print(" FR Limit: ");
     Serial.println(Stepper1LimitPinFlag);

     Serial.print("FL Distance to go: ");
     Serial.print(Stepper2.distanceToGo());
     Serial.print(" Current Position: ");
     Serial.print(Stepper2.currentPosition());
     Serial.print(" FL Limit: ");
     Serial.println(Stepper2LimitPinFlag);

     Serial.print("RR Distance to go: ");
     Serial.print(Stepper3.distanceToGo());
     Serial.print(" Current Position: ");
     Serial.print(Stepper3.currentPosition());
     Serial.print(" RR Limit: ");
     Serial.println(Stepper3LimitPinFlag);

     Serial.print("RL Distance to go: ");
     Serial.print(Stepper4.distanceToGo());
     Serial.print(" Current Position: ");
     Serial.print(Stepper4.currentPosition());
     Serial.print(" RL Limit: ");
     Serial.println(Stepper4LimitPinFlag);
     Serial.println("");
}

void R4_homeSteering()
{ 
  Serial.print ("Start:");
  R4_displayLimits();
  Serial.println  ("Moving towards switch:");
     
  Stepper1.moveTo (homingDistance);
  Stepper2.moveTo (-homingDistance);
  Stepper3.moveTo (homingDistance);
  Stepper4.moveTo (-homingDistance);
   
  while(!Stepper1LimitPinFlag || !Stepper2LimitPinFlag || !Stepper3LimitPinFlag || !Stepper4LimitPinFlag )
  {
   R4_updateLimits();
   if(!Stepper1LimitPinFlag) Stepper1.run(); else Stepper1.stop();  
   if(!Stepper2LimitPinFlag) Stepper2.run(); else Stepper2.stop();
   if(!Stepper3LimitPinFlag) Stepper3.run(); else Stepper3.stop();
   if(!Stepper4LimitPinFlag) Stepper4.run(); else Stepper4.stop();
   
   if(dellay++>50)
    {        
     R4_displayLimits();
     dellay = 0;
    }
  } 

  Serial.println  ("Moving towards switch final positions:"); 
  R4_displaySteeringStateNow();
     
  Stepper1.setCurrentPosition(0);
  Stepper2.setCurrentPosition(0);
  Stepper3.setCurrentPosition(0);
  Stepper4.setCurrentPosition(0);

  Stepper1.run(); 
  Stepper2.run();
  Stepper3.run();
  Stepper4.run();
  
  Stepper1.moveTo (-homingDistance);
  Stepper2.moveTo (homingDistance);
  Stepper3.moveTo (-homingDistance);
  Stepper4.moveTo (homingDistance);

  Stepper1.run(); 
  Stepper2.run();
  Stepper3.run();
  Stepper4.run();
   
  Serial.print ("Move away from switch:");
  R4_displayLimits();

  R4_displaySteeringStateNow();
  
  while(Stepper1LimitPinFlag || Stepper2LimitPinFlag || Stepper3LimitPinFlag || Stepper4LimitPinFlag )
  {
   R4_updateLimits();
   if(Stepper1LimitPinFlag) Stepper1.run(); else Stepper1.stop(); 
   if(Stepper2LimitPinFlag) Stepper2.run(); else Stepper2.stop();
   if(Stepper3LimitPinFlag) Stepper3.run(); else Stepper3.stop();
   if(Stepper4LimitPinFlag) Stepper4.run(); else Stepper4.stop();

   if(dellay++>50)
   {   
    R4_displayLimits();
    dellay = 0;
   }
  }  

  Stepper1.setCurrentPosition(0);
  Stepper2.setCurrentPosition(0);
  Stepper3.setCurrentPosition(0);
  Stepper4.setCurrentPosition(0);
  
  Serial.print("Complete:");
  R4_displayLimits();

  Stepper1.moveTo (-Stepper1CentrePos); 
  Stepper2.moveTo (Stepper2CentrePos); 
  Stepper3.moveTo (-Stepper3CentrePos); 
  Stepper4.moveTo (Stepper4CentrePos);
   
  while(Stepper1.distanceToGo() != 0 || Stepper2.distanceToGo() != 0 || Stepper3.distanceToGo() != 0 || Stepper4.distanceToGo() != 0 )
  {
   R4_updateLimits();

   if(Stepper1.distanceToGo() != 0) Stepper1.run();  
   if(Stepper2.distanceToGo() != 0) Stepper2.run();
   if(Stepper3.distanceToGo() != 0) Stepper3.run();
   if(Stepper4.distanceToGo() != 0) Stepper4.run();
   
   if(dellay++>50)
   {   
    Serial.print("Setting Centre:");
    R4_displayDisToGo();
    dellay = 0;
   }
  } 
  dellay = 100000;
  Serial.println("");
}

void R4_runSteppers()
{
  Stepper1.run();  
  Stepper2.run();
  Stepper3.run();
  Stepper4.run();
}

