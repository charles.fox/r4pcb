// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 PODCar Example 

#include "R4_Def.h"
#include <RPC.h>
#include <Arduino.h>
#include <Wire.h>
#include "R4_Steppers.h"
#include "R4_ServoBank.h"
#include "R4_Timer.h"
#include "R4_Analog.h"
#include "R4_Relays.h"
#include "R4_DHB12HBridge.h"
#include "R4_OSMCHBridge.h"
#include "R4_DMH.h"
#include "R4_ROS2_UDP.h"

#define MotorTest
int  count       = 10;        // H bridge test code variable

R4_Analog  R4_Analog;
podcar_data_type podcar_data;

// Loop time variables
long t1          =  0;        // Start time of loop in millis
long oldtime     =  0;
int  lt          =  0;        // delta T between previous start time and latest loop start time

int  loopcounter =  0;
bool d           =  0;
 
bool sendTimedDataPacket         = 0;   
bool oldsendTimedDataPacket      = 0;
int  dataPacketISRcount          = 0;
bool sendTimedheartBeatPacket    = 0;  
bool oldsendTimedheartBeatPacket = 0;
int  heartBeatPacketISRcount     = 0;
bool oldsendTimedUDPPacket       = 0;
int  sendcount                   = 1; 
bool haltState                   = true;
bool stopCondition               = false;

String HeartBeat = "H:R4-ROS2;";

int  Hbeat                     = 0;
int  oldHbeat                  = 0;
int  timeBetweenHeartbeats     = 0;
int  HeartBeatInterval         = 1050;    //expected maximum interval in milli seconds before a heart beat late state is induced.
int  heartBeatisLateCount      = 0;
bool heartBeatHappened         = false;
bool heartBeatCRCfailed        = false;
int  averageHeartBeatTime      = 0;
int  averagerCount             = 0;       //heatbeat interval averaging variables
int  lastaverage               = 0;      

int servo_Duration             = 1000;

void setup()  
{ 
  R4_setup();              // Setup Pins, Timers, i2C etc
  R4_ServoBank_Init();     // Init the Servo motor library
  R4_SteppersInit();       // Init the Stepper Motor Library
  R4_StartSteppers();      // further stepper initilization
  R4_Relay_Init();         // Enable the 8 Channel Relay Board
  R4_DHB12_Init();         // Initialize the dhb12 H Bridge interface
  R4_DHB12_1_A_Enable();   // Enable dhb12_1A
  R4_OSMC_Init();          // Init osmc1
  R4_OSMC1_Enable();       // Enable osmc_1
  R4_DMH_Init();           // Enable the DMH relay
  R4_DMH_Relay_On();       // Switch the DMH relay on
  R4_ROS2_UDP_Init();      // Enable ROS2 Wifi UDP comms Protocol
}
 
void loop() 
{
  R4_Measure_Loop_Time();

  if(!haltState && !stopCondition)
    R4_MotorTestCode();

  R4_Do_UDP_Comms();
}
  
void R4_beginSerial()
{
  #ifdef DEBUGSerial
   Serial.begin(1000000);
   delay(1000);                //so the serial terminal doesnt miss anything on start up
   Serial.println("");
   Serial.println("R4 Test!");
   Serial.println("");
  #endif
  
  #ifdef DEBUGTFT
   Serial2.begin(250000);
  #endif
}

void R4_setupPins()
{
  pinMode     (GPIO43LEDpin  , OUTPUT);
  pinMode     (GPIO46LEDpin  , OUTPUT);
  digitalWrite(GPIO46LEDpin  ,  HIGH);
  digitalWrite(GPIO46LEDpin  ,  HIGH);
}
 
void R4_setup()
{
  R4_beginSerial();       // Start Serial0 at 1000000 baud and Uart1 at 250000 baud
  R4_scan_I2cBus();       // Scan the i2C bus and make sure all the interface IC's are responding
  R4_setupPins();         // Initialize the R4 LED's on GPIO46 and GPIO 43
  R4_setupTimers();       // Initialize the 500kHz timer and the 1Hz sub timer
}

void R4_Measure_Loop_Time()
{
  t1 = millis();
  lt = t1 - oldtime;
  oldtime = t1;
}

/******************************** UDP Comms *******************************************/

void R4_Do_UDP_Comms()
{
  if(WiFi.status() == WL_CONNECTED && !haltState)                              //Check on every loop if the Wifi is still connected and that a halt condition has not been set due to heatbeat failure, in either case stop all the motors
  {  
    String currentPacket = R4_ROS2_UDP_Client();  
    if(currentPacket.length() > 0)
     R4_processUDPPacket(currentPacket);                                       //Deal with any incoming UDP packets i.e. Heartbeat or Command.                                       
    R4_Time_HeartBeat_Packets();                                               //Calcualte the interval between received heartbeat packets
    R4_GetPodCarData();                                                           //Read all the podcar related data from the hardware 
  
     // Send the Heartbeat message "R4-ROS2" once per second
    if(sendTimedheartBeatPacket != oldsendTimedheartBeatPacket)                // 1 Hz heartbeat message (assumes 100Hz packet rate for podcar data)
    { 
      #ifdef DEBUGSerial  
        Serial.print("Send Counter: ");
        Serial.println(sendcount);                                             //Display releveant serial messages on the debug uart
      #endif            
      R4_ROS2_SendUDPPacket(HeartBeat);
      oldsendTimedheartBeatPacket = sendTimedheartBeatPacket;
    } 

    // Send the current podcar data set to ROS2 100 times per second
    if(sendTimedDataPacket != oldsendTimedDataPacket)                           // 100Hz podcar data - rate controlled by Timer ISR (see Timer.h for interval and the TimerHandler function below for the divider)
    {   
      R4_makeUDPsendPacket(podcar_data);                                      
      #ifdef DEBUGSerial                                                      // Display releveant serial messages on the debug uart
        SerialDebug();
      #endif  
      oldsendTimedDataPacket = sendTimedDataPacket;  
    }
  } 
   else
    {
     // Wifi Link dropped out or Heartbeat failed, so pull all hardware into a safe state (stop motors, switch off relay, disable DMH etc)
     R4_Stop();

     #ifdef DEBUGSerial
      Serial.println("Link Failed - Saftey Shutdown");
     #endif

     if(WiFi.status() == WL_CONNECTED)
     {
       loopcounter          = 0;
       haltState            = false;            // remove the halt state
       lastaverage          = 0;
       heartBeatisLateCount = 0;                // re-initialise the heartBeat variables
       heartBeatHappened    = false;
       heartBeatCRCfailed   = false;
       
       if(!stopCondition)
       {
        R4_ROS2_UDP_Connect();                   // if the wifi link is still connected, attempt to re-establish a heartbeat                                         
        R4_Start();                              // recover started
       }
       else
       {
        R4_ROS2_UDP_Connect();                   // if the wifi link is still connected, attempt to re-establish a heartbeat                                         
        R4_Stop();                               // recover stopped
       }
      }
      else
      {
        #ifdef DEBUGSerial
          Serial.println("Wifi Dropped Out - Saftey Shutdown Reset Microcontroller required.");
        #endif
        while(true){};    // If we ended up here the wifi connection dropped out altoegther
        // So Stop Everything for now - this forces a microcontroller reset
        // Ideally we attemp to recconect to a wifi network (several SSID's could be provided to search through)
      }
    }
}

bool R4_makeUDPsendPacket(podcar_data_type data)
{
  // probably best to move this to R4_ROS2_UDP.cpp file not sure
  String UDPsendPacket = "";
  UDPsendPacket = UDPsendPacket + "AIN24:"    + String(data.Reading_24V,2) +";";
  UDPsendPacket = UDPsendPacket + "AIN12:"    + String(data.Reading_12V,2) +";";
  UDPsendPacket = UDPsendPacket + "AIN5:"     + String(data.Reading__5V,2) +";"; 
  UDPsendPacket = UDPsendPacket + "AINDMH:"   + String(data.Reading_DMH,2) +";";
  UDPsendPacket = UDPsendPacket + "DMHSTA:"   + String(data.DMHRelayState) +";";
  UDPsendPacket = UDPsendPacket + "AINSTEER:" + String(data.Steer_FBack,2) +";";
  UDPsendPacket = UDPsendPacket + "DHB1A:"    + String(data.dhb12_1A.Duration) + "," + String(data.dhb12_1A.Direction) +";";
  UDPsendPacket = UDPsendPacket + "OSMC1:"    + String(data.osmc_1.Duration)   + "," + String(data.osmc_1.Direction)   +";";
  UDPsendPacket = UDPsendPacket + "STEP1POS:" + String(R4_Stepper_getPosition(1)) + ";";
  UDPsendPacket = UDPsendPacket + "SERVO1POS:"+ String(servo_Duration) + ";"; //String(R4_ServoBank_getChannel_ServoPulseDuration(0)) + ";";
  return R4_ROS2_SendUDPPacket(UDPsendPacket);
}

void R4_processUDPPacket(String packet)
{
  // probably best to move this to R4_ROS2_UDP.cpp file not sure (could all be moved to the wifi firmware or UDP lib!)

  #ifdef DEBUGSerial
   Serial.println("Packet Being Processed: " + packet);
  #endif

  String cmd              = "";
  String data             = "";
  String data1            = "";
  String data2            = "";
  String data3            = "";
  String packetWithoutCRC = "";
  String packetCRC        = "";

  cmd  = packet.substring(0,1);
  data = packet.substring(2,packet.length());
  
  // extract the CRC
  int endofPacket  = packet.indexOf("^");
  packetWithoutCRC = cmd + ":" + data.substring(0,packet.indexOf(";") - 1);
  packetCRC        = packet.substring(packet.indexOf(";") + 1 , endofPacket);

  #ifdef DEBUGSerial 
    Serial.println("Packet Checked: " + packetWithoutCRC);
    Serial.println("Received CRC32: " + packetCRC);
  #endif 
  
  if(R4_ROS2_checkPacketCRC(packetWithoutCRC, packetCRC))      // if the CRC is valid for this packet process the packet
  {
    switch(cmd[0])
    {
      case 'H': // Heartbeat Signal from UDP Server
        // FORMAT = H: String "ROS2-R4",uint32_t CRC32^
        // EXAMPLE: H:ROS2-R4;0xabcd1234^
      {
        data1 = data.substring( 0 , data.indexOf(";"));      

        if(data1 == "ROS2-R4")
        {   
          heartBeatHappened = true;  
          #ifdef DEBUGSerial 
           Serial.print  ("Time Between Heartbeats: ");
           Serial.println(timeBetweenHeartbeats);
           Serial.print  ("Heartbeat CRC Failed: ");
           Serial.println(heartBeatCRCfailed);
          #endif  
        }  
      }
      break;

      case 'D':  // Set DHB12 speed and direction
      // FORMAT = D:int duration in milliseconds,bool direction, int channel;uint32_t CRC32^
      // EXAMPLE: D:560,1,1 to 4;0xabcd1234^
      {
        packetWithoutCRC = packetWithoutCRC.substring(2, packetWithoutCRC.indexOf(";"));                  //remove the command section
        data1 = packetWithoutCRC.substring(0, packetWithoutCRC.indexOf(","));                             //get the first parameter
        packetWithoutCRC = packetWithoutCRC.substring(data1.length() + 1,packetWithoutCRC.length());      //remove the first parameter
        data2 = packetWithoutCRC.substring(0, packetWithoutCRC.indexOf(","));                             //get the second parameter          
        data3 = packetWithoutCRC.substring(data2.length() + 1,packetWithoutCRC.indexOf(";"));             //remove the second parameter and get the third parameter 

        int  speed   = data1.toInt();
        bool dir     = (bool)data2.toInt();
        int  channel = data3.toInt(); 

        switch(channel)
        {
         case 1:
          // scaled speed is a temp fix for incoming speed signal range being 0 to 199 (this needs changing to 0 to 4095)
          R4_DHB12_1_A_setSpeedandDirection(speed,dir);
          break;
         case 2:
          R4_DHB12_1_B_setSpeedandDirection(speed,dir);
          break;
         case 3:
          R4_DHB12_2_A_setSpeedandDirection(speed,dir);
          break;
         case 4:
          R4_DHB12_2_B_setSpeedandDirection(speed,dir);
          break;
         default:
          #ifdef DEBUGSerial
           Serial.println("Channel Error on DHB12 command request");
          #endif
        }
      }
      break;

      case 'O': // Set OSMC speed and direction  
      // FORMAT = O:int duration in milliseconds,bool direction, int channel;uint32_t CRC32^
      // EXAMPLE: O:560,1,1;0xabcd1234^
      {
        packetWithoutCRC = packetWithoutCRC.substring(2, packetWithoutCRC.indexOf(";"));                  //remove the command section
        data1 = packetWithoutCRC.substring(0, packetWithoutCRC.indexOf(","));                             //get the first parameter
        packetWithoutCRC = packetWithoutCRC.substring(data1.length() + 1,packetWithoutCRC.length());      //remove the first parameter
        data2 = packetWithoutCRC.substring(0, packetWithoutCRC.indexOf(","));                             //get the second parameter          
        data3 = packetWithoutCRC.substring(data2.length() + 1,packetWithoutCRC.indexOf(";"));             //remove the second parameter and get the third parameter 

        int  speed   = data1.toInt();
        bool dir     = (bool)data2.toInt();
        int  channel = data3.toInt(); 
        
        switch(channel)
        {
         case 1:
          R4_OSMC1_setSpeedandDirection(speed,dir);
          break;
         case 2:
          R4_OSMC2_setSpeedandDirection(speed,dir);
          break;
         case 3:
          R4_OSMC3_setSpeedandDirection(speed,dir);
          break;
         case 4:
          R4_OSMC4_setSpeedandDirection(speed,dir);
          break;
         default:
          #ifdef DEBUGSerial
           Serial.println("Channel Error on OSMC command request");
          #endif
        }
      }
      break;

      case 'R': // Set DMH relay state 
      // FORMAT = R:bool state;uint32_t CRC32^ 
      // EXAMPLE: R:On;0xabcd1234^
      {
        if(data=="On")
          R4_DMH_Relay_On();
          else
            R4_DMH_Relay_Off();
      }
      break;  

      case 'S': // ROS2 instructed a full stop - all hardware should go into a safe state - motors stop.
      // FORMAT = R:String "STOP";uint32_t CRC32^ 
      // EXAMPLE: S:STOP;0xabcd1234^       
      {
        data1 = data.substring( 0 , data.indexOf(";")); 
        if(data1 == "STOP")
        {
         stopCondition = true;
         R4_Stop();
         #ifdef DEBUGSerial
          Serial.print("Stop Condition = ");
          Serial.println(stopCondition);
         #endif
        }

        if(data1 == "START")
        {
         stopCondition = false;
         R4_Start();
         #ifdef DEBUGSerial
          Serial.print("Stop Condition = ");
          Serial.println(stopCondition);
         #endif
        }
      }
      break;

      default:
      {
       // unknown packet type - need to think about what to do with this case. Maybe just NACK it beacuse it was not understood.
       #ifdef DEBUGSerial 
        Serial.println("Error Unkownn Command Packet: " + packet);
       #endif 
      }
      break;
    }
  }
   else
    {
      //CRC Failed           
      #ifdef DEBUGSerial 
        Serial.println("HeartBeat CRC Failed:" + packet);
      #endif 
      // so set the flag.
      heartBeatCRCfailed = true;
     }
}

void R4_GetPodCarData()
{   
    podcar_data.Reading_24V   = R4_Analog.ReadAIN1();                          //Read the incoming 24v from the battery
    podcar_data.Reading_12V   = R4_Analog.ReadAIN2();                          //Read the incoming 12v from the 24 to 12 buck converter  
    podcar_data.Reading__5V   = R4_Analog.ReadAIN3();                          //Read the incoming  5v from the 24 to  5 buck converter
    podcar_data.Reading_DMH   = R4_Analog.ReadAIN4();                         //Read the incoming DMH Circuit
    podcar_data.Steer_FBack   = R4_Analog.ReadAIN5();                          //Allocate AIN5 to the steering feedback signal - apply 3.3V or 5v to the pot and calibrate the calculation in Analog.cpp
    podcar_data.DMHRelayState = digitalRead(DMHRelay_GPIOpin);
    podcar_data.dhb12_1A      = R4_DHB12_getSpeedandDirection(1);
    podcar_data.osmc_1        = R4_OSMC_getSpeedandDirection(1);
}

void R4_Time_HeartBeat_Packets()
{
  Hbeat = millis();

  if(heartBeatHappened)
  {
   timeBetweenHeartbeats = Hbeat - oldHbeat;
   oldHbeat = Hbeat; 
   heartBeatHappened = false;

   averageHeartBeatTime = averageHeartBeatTime + timeBetweenHeartbeats;
   
   // Should also measure the standard deviation! Means carrying more data that probably will not get used by R4 later
   // but usefull for characterising the link behavour.

   // Implement the following:

   // Step 1: Find the mean. (Already done)
   // Step 2: For each data point, find the square of its distance to the mean.
   // Step 3: Sum the values from Step 2.
   // Step 4: Divide by the number of data points. (Window size)
   // Step 5: Take the square root.

   averagerCount++;

   if(averagerCount == 10)
   {
    lastaverage = averageHeartBeatTime / 10;  
    averageHeartBeatTime = 0;
    averagerCount = 0; 
   }

  }
  else
  {
    timeBetweenHeartbeats = Hbeat - oldHbeat;
  }
  
  if (timeBetweenHeartbeats > HeartBeatInterval || heartBeatCRCfailed)
  {
    heartBeatisLateCount++;

    #ifdef DEBUGSerial                                                      // Display releveant serial messages on the debug uart
      Serial.println("Heart Beat late: " + timeBetweenHeartbeats);
      if(heartBeatCRCfailed)
       Serial.println("Because the CRC Failed");
    #endif 

    heartBeatCRCfailed = false;

    if(heartBeatisLateCount > 2)
    {   
        #ifdef DEBUGSerial
          Serial.print("Heart Beat signal was late more than twice: ");
          Serial.print(timeBetweenHeartbeats);
          Serial.println(" ms");
        #endif
        haltState = true;
    }
  } 
}

/******************************** Start or Stop ALL the R4 interfaces *******************************************/

void R4_Start()
{
  R4_DMH_Relay_On();                       // if the connect call returns we know we can resume
  R4_Relay_setAllRelayPins_Off();          // restablish a running state
  R4_DHB12_1_A_Enable();  
  R4_OSMC1_Enable(); 
  R4_StartSteppers();
  //R4_ServoBank_Wakeup();
}

void R4_Stop()
{
  R4_StopSteppers();
  R4_DMH_Relay_Off();
  R4_Relay_setAllRelayPins_Off();
  R4_DHB12_DisableAll();
  R4_OSMC1_Disable();
  //R4_ServoBank_Sleep();
}

/******************************** TIMERS SETUP and ISR *******************************************/
void R4_setupTimers()
{
  // Interval in microseconds
  if (ITimer0.attachInterruptInterval(TIMER0_INTERVAL_uS, R4_TimerHandler))
  {
    #ifdef DEBUGSerial
      Serial.println("Started ITimer0 OK");
      Serial.println("");
    #endif
  }
   else
   {
    #ifdef DEBUGSerial
      Serial.println("Can't set ITimer0");
      Serial.println("");
    #endif
   }
}

void R4_TimerHandler() 
{
  // This ISR Occurs 1000 times per second (1KHz - Period 1mS)  // although a scope will see half this on pin GPIO46 because the pin is toggled in the interrupt and only produces a half cycle transition per interrupt.

  if(!haltState) // && #if defined(R4_STEPPERS_H))                //if you include the stepper motor library this will be executed other not. 
   R4_runSteppers();  

  if(++dataPacketISRcount == 10)                                        
   { 
     // 100 times per second
     sendTimedDataPacket = !sendTimedDataPacket;   
     dataPacketISRcount = 0;  
   }

  if(++heartBeatPacketISRcount == 1000)                                        
   { 
     // Once per second
     sendTimedheartBeatPacket = !sendTimedheartBeatPacket;   
     heartBeatPacketISRcount = 0;  
   }

  // add another for TFT if needed
  // outputTFT = true;                                           // this sub counter sets the send to TFT flag every 10 ISR's (10 x 10mS = Period 100mS or 10Hz aprrox update rate for TFT display - (the TFT data is sent in normal time not ISR time)
  // toggle the state of the TFT debug pin A0 
}

/******************************** I2C Bus Scanner *******************************************/

void R4_scan_I2cBus()
{
  Wire.begin();
  byte error, address;
  int nDevices;
  #ifdef DEBUGSerial
    Serial.println("Scanning i2c Bus...");
    nDevices = 0;
    for(address = 1; address < 127; address++ ) 
    {
      Wire.beginTransmission(address);
      error = Wire.endTransmission();

      if (error == 0)
      {
        Serial.print("i2c device found at address 0x");
        if (address<16) 
          Serial.print("0");
        Serial.println(address,HEX);
        nDevices++;
      }
      else if (error==4) 
      {
        Serial.print("Unknown error at address 0x");
        if (address<16) 
          Serial.print("0");
        Serial.println(address,HEX);
      }    
    }
    if (nDevices == 0)
      Serial.println("i2c not found");
    else
      Serial.println("Scan Complete");
  #endif
}

/******************************** Motor Test *******************************************/

void R4_MotorTestCode()
{
  if(++loopcounter > 20000)  // on the 20001 st time execute the motor code.
  {
   loopcounter = 1;
    #ifdef MotorTest
      // every 20000 loops send the servo the the opposite end of its travel
      if (servo_Duration == 2000)
        servo_Duration=1000;
        else
          servo_Duration = 2000;                                                // 180 degrees total movement from 1000uS to 2000uS. 
                                                                                // You can stretch the width of the signal for more angular travel (dont over do it though <2500us)
      R4_ServoBank_setChannel_ServoPulseDuration(servo_Duration,0);             // channel and duration 900 to 2100 in library, range needs testing!
      
      //TestCode to bounce the Stepper motors between two step values
      R4_BounceSteppers();
    #endif
  }
}






