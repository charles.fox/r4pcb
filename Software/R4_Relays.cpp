// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 8 Channel Relay

#include "R4_Relays.h"

DFRobot_MCP23017 R4_Rel(Wire, R4_Relays_device_Address);

/*BLDC DIR and BRK Pins
R4_Rel.digitalWrite(mcp.eGPA0, HIGH);
R4_Rel.digitalWrite(mcp.eGPA1, HIGH);
R4_Rel.digitalWrite(mcp.eGPA2, HIGH);
R4_Rel.digitalWrite(mcp.eGPA3, HIGH);
R4_Rel.digitalWrite(mcp.eGPA4, HIGH);
R4_Rel.digitalWrite(mcp.eGPA5, HIGH);
R4_Rel.digitalWrite(mcp.eGPA6, HIGH);
R4_Rel.digitalWrite(mcp.eGPA7, HIGH);
*/

void R4_Relay_Init()
{
  while(R4_Rel.begin() != 0)
  {
    Serial.println("Initialization of the chip failed, please confirm that the chip connection is correct!");
    delay(1000);
  }
  R4_Rel.pinMode(R4_Rel.eGPB, OUTPUT);
  
  R4_Relay_setAllRelayPins_Off();
}

void R4_Relay_Test()
{
  R4_Relay_setRelayPin_On_Off(1, AllOn);
  delay(100);
  R4_Relay_setRelayPin_On_Off(2, AllOn);
  delay(100);
  R4_Relay_setRelayPin_On_Off(3, AllOn);
  delay(100);
  R4_Relay_setRelayPin_On_Off(4, AllOn);
  delay(100);
  R4_Relay_setRelayPin_On_Off(5, AllOn);
  delay(100);
  R4_Relay_setRelayPin_On_Off(6, AllOn);
  delay(100);
  R4_Relay_setRelayPin_On_Off(7, AllOn);
  delay(100);
  R4_Relay_setRelayPin_On_Off(8, AllOn);
  delay(100);

  R4_Relay_setRelayPin_On_Off(8, AllOff);
  delay(100);
  R4_Relay_setRelayPin_On_Off(7, AllOff);
  delay(100);
  R4_Relay_setRelayPin_On_Off(6, AllOff);
  delay(100);
  R4_Relay_setRelayPin_On_Off(5, AllOff);
  delay(100);
  R4_Relay_setRelayPin_On_Off(4, AllOff);
  delay(100);
  R4_Relay_setRelayPin_On_Off(3, AllOff);
  delay(100);
  R4_Relay_setRelayPin_On_Off(2, AllOff);
  delay(100);
  R4_Relay_setRelayPin_On_Off(1, AllOff);
  delay(100);
}

void R4_Relay_setAllRelayPins_On()
{
 R4_Rel.digitalWrite(R4_Rel.eGPB, AllOn);
}

void R4_Relay_setAllRelayPins_Off()
{
 R4_Rel.digitalWrite(R4_Rel.eGPB, AllOff);
}

void R4_Relay_setRelayPin_On_Off(int pin, bool On_Off)
{
  switch (pin) 
  {
    case 1:
      R4_Rel.digitalWrite(R4_Rel.eGPB7, On_Off);
      break;
    case 2:
      R4_Rel.digitalWrite(R4_Rel.eGPB6, On_Off);
      break;
    case 3:
      R4_Rel.digitalWrite(R4_Rel.eGPB5, On_Off);
      break;
    case 4:
      R4_Rel.digitalWrite(R4_Rel.eGPB4, On_Off);
      break;
    case 5:
      R4_Rel.digitalWrite(R4_Rel.eGPB3, On_Off);
      break;
    case 6:
      R4_Rel.digitalWrite(R4_Rel.eGPB2, On_Off);
      break;
    case 7:
      R4_Rel.digitalWrite(R4_Rel.eGPB1, On_Off);
      break;
    case 8:
      R4_Rel.digitalWrite(R4_Rel.eGPB0, On_Off);
      break;
    default:
      break;
  }   


}
