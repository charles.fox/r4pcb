// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 Analog Pin defines

#include <Arduino.h>

#ifndef R4_ANALOG_H
  #define   R4_ANALOG_H

  #define AIN1pin    A6   // 24V Fused  J1
  #define AIN2pin    A7   // 12V Fused	J2
  #define AIN3pin    A8   // 5V Fused	  J3
  #define AIN4pin    A9   // DMH_IN     J4
  #define AIN5pin   A10   // no Jumper function
  #define AIN6pin   A11   // no Jumper function

#endif

  // ********** INTERFACE *********************************************************************************************************

  #ifndef R4_Analog_h
    #define R4_Analog_h
    #include "Arduino.h" 

    class R4_Analog 
    {
      public:   
        R4_Analog();
        double ReadAIN1();
        double ReadAIN2();
        double ReadAIN3();
        double ReadAIN4();
        double ReadAIN5();
        double ReadAIN6();
    };
  #endif