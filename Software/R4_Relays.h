// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 8 Channel Relay


  // https://github.com/DFRobot/DFRobot_MCP23017
  #include <DFRobot_MCP23017.h>
  #define R4_Relays_device_Address 0x20

#ifndef R4_RELAYS_H
  #define R4_RELAYS_H
  #define AllOn    0x00 //Relay board on off is flipped 0 means on
  #define AllOff   0xFF

  void R4_Relay_Init();
  void R4_Relay_Test();
  void R4_Relay_setRelayPin_On_Off(int pin, bool On_Off);
  void R4_Relay_setAllRelayPins_On();
  void R4_Relay_setAllRelayPins_Off();
#endif
	