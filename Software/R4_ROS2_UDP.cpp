// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 ROS2 WIFI UDP

#include "R4_ROS2_UDP.h"
#include <Arduino_CRC32.h> //Alexander Entinger
 
WiFiUDP wifiUDP;
static bool GPIO43state = 0;

IPAddress udpServer = (172,20,10,7);   //set the ip address of the server here.

String UDPbuffer;
String ConnectionPacket = "";

Arduino_CRC32 crc32;
uint16_t localPort = 2018; 

void R4_ROS2_UDP_Init() 
{
  #ifdef DEBUGSerial
   Serial.println("Initializing Giga WiFi UDP Client");
  #endif

  WiFi.begin(SSID, SSID_PW);
  while (WiFi.status() != WL_CONNECTED) {};  

  Serial.println(WiFi.localIP());
  Serial.print("Connected to Wifi: ");   Serial.print(SSID); Serial.print(" : "); Serial.println("WL_CONNECTED"); 
  Serial.print("Starting Wifi UDP - 0 means failed: ");      Serial.println(wifiUDP.begin(localPort));                //UDP begin, local port set here.
  
  R4_ROS2_UDP_Connect(); 
}

void R4_ROS2_UDP_Connect()
{
  ConnectionPacket = "";

  while(ConnectionPacket != "H:ROS2-R4;")       /// make sure this gets CRC'd **************************************************************************
  {
    // need to stop the stepper somehow in here TODO
    ConnectionPacket = R4_ROS2_UDP_Client();
    ConnectionPacket = ConnectionPacket.substring(0,10);
    #ifdef DEBUGSerial
      Serial.print  ("Sending Connection Packet: ");
    #endif
    R4_ROS2_SendUDPPacket("H:R4-ROS2;"); 
    delay(1000);
  }
}

String R4_ROS2_UDP_Client() 
{
  char UDPacketBuffer[UDP_TX_PACKET_MAX_SIZE];                // temp buffer to hold incoming packet
  int packetLength = wifiUDP.parsePacket();
  if (packetLength > 0)
  { 
    wifiUDP.read(UDPacketBuffer, packetLength);
    String packet = String(UDPacketBuffer,packetLength);
    #ifdef DEBUGSerial
     Serial.println("Incoming Packet As Received:" + packet);
    #endif   
    digitalWrite(GPIO43LEDpin , !digitalRead(GPIO43LEDpin));  // indicate on the GPIO46 that a packet has been recceived
    return packet;                                            
  } 
  else 
   return "";
}

bool  R4_ROS2_SendUDPPacket(String packet)
{
  uint32_t crc32_res = crc32.calc((uint8_t const *)packet.c_str(), strlen(packet.c_str()));
  //packet = "T:" + String(millis()) + ";" + packet;          // Add timestamp
  packet = packet + "0x" + String(crc32_res, HEX) + "^";      // Add a CRC32 data element to the end of the outgoing packet
  wifiUDP.beginPacket(wifiUDP.remoteIP(), UDP_REMOTE_PORT);
  wifiUDP.write(packet.c_str());       
  digitalWrite(GPIO46LEDpin, !digitalRead(GPIO46LEDpin));
  #ifdef DEBUGSerial
    Serial.println("Sent to ROS2: " + packet);
    Serial.println();
  #endif
  if(!wifiUDP.endPacket())                                   
   {
    #ifdef DEBUGSerial
     Serial.println("WifiUDP.endPacket failed! Error");
    #endif
    return 0;
   }
   else
    return 1; 
}

bool R4_ROS2_checkPacketCRC(String checkPacketincoming, String packetCRCincoming)
{
  uint32_t crc32_res = crc32.calc((uint8_t const *)checkPacketincoming.c_str(), strlen(checkPacketincoming.c_str()));
  String calculatedPacketCRC = "0x" + String(crc32_res, HEX);
  if(packetCRCincoming == calculatedPacketCRC)
  {
   #ifdef DEBUGSerial
    Serial.println("CRC Correct");
   #endif
   return 1;
  }
    else
    {
     #ifdef DEBUGSerial
      Serial.println("Calculated CRC :" +  calculatedPacketCRC);
      Serial.println("CRC Failed!");
      //while(true){};
     #endif     
     return 0;
    }
}
