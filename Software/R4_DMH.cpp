// Chris Waltham 13-07-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 DMH - this handles the Dead Mans Handle Switch and relay

#include "R4_DMH.h"

void R4_DMH_Init()
{
  pinMode(DMHRelay_GPIOpin,OUTPUT);
  digitalWrite(DMHRelay_GPIOpin,LOW);
}

void R4_DMH_Relay_On()
{
  digitalWrite(DMHRelay_GPIOpin,HIGH);
}

void R4_DMH_Relay_Off()
{
  digitalWrite(DMHRelay_GPIOpin,LOW);
}

double R4_DMH_ReadVoltage()
{
    // read the AIN4 analog pin
  int val = analogRead(AIN4pin);        
  double voltage = ((double)val/1024) * 14.2;           // Calculate the Voltage 
  return voltage;
}