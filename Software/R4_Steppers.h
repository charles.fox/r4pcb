// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// Stepper Functions for the R4 Board

#include <Arduino.h>

//https://www.arduino.cc/reference/en/libraries/accelstepper/
#include <AccelStepper.h>

#ifndef ACCELSTEPPER_H
 #define ACCELSTEPPER_H
#endif

#ifndef R4_STEPPERS_H
 #define R4_STEPPERS_H

  // Stepper Motor Pins for the R4
  #define Stepper1StepPin         51   // D51	R4 PUL-(PUL)_1	Stepper_1 
  #define Stepper1DirPin          50   // D50	R4 DIR-(DIR)_1	Stepper_1
  #define Stepper2StepPin         48   // D48	R4 PUL-(PUL)_2	Stepper_2
  #define Stepper2DirPin          47   // D47	R4 DIR-(DIR)_2	Stepper_2
  #define Stepper3StepPin         45   // D45	R4 PUL-(PUL)_3	Stepper_3
  #define Stepper3DirPin          44   // D44	R4 DIR-(DIR)_3	Stepper_3
  #define Stepper4StepPin         42   // D42	R4 PUL-(PUL)_4	Stepper_4
  #define Stepper4DirPin          41   // D41	R4 DIR-(DIR)_4	Stepper_4

  // Steering Limit Switch Pins
  #define Stepper1LimitPin         0  // R4 Encoder B Pins can be used for used for limit switches 
  #define Stepper2LimitPin         0  // These have not been defined here yet
  #define Stepper3LimitPin         0
  #define Stepper4LimitPin         0

  // All Motors Enable Pin for the R4
  #define enablePin                49  // D49 R4 shield Enable Pin, all motors (must be defined after AccelStepper)

  // ********** INTERFACE *********************************************************************************************************

    void R4_SteppersInit();
    void R4_StartSteppers();
    void R4_StopSteppers();
    long R4_Stepper_getPosition(int channel);
    
    void R4_BounceSteppers();
    void R4_updateLimits();
    void R4_displayLimits();
    void R4_displayDisToGo();
    void R4_displaySteeringState();
    void R4_displaySteeringStateNow();
    void R4_homeSteering();
    void R4_runSteppers();
    
 #endif