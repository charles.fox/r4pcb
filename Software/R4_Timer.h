// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// STM32F4 Timer and ISR setup (Portenta_H7 Library)

// The Portenta_H7 Timer Library needs editing to use this on the Giga - a compiler trap for the board prevents the code compiling for the Giga.

#define _TIMERINTERRUPT_LOGLEVEL_     4    // _TIMERINTERRUPT_LOGLEVEL_ from 0 to 4

//https://www.arduino.cc/reference/en/libraries/portenta_h7_timerinterrupt/
#include "Portenta_H7_TimerInterrupt.h"
#define TIMER0_INTERVAL_uS         1000   // This value is in microseconds so this means the timers ISR will trigger 500 times per second
Portenta_H7_Timer ITimer0(TIM15);

#ifndef R4TIMER_H
 #define R4TIMER_H
#endif

// In Portenta_H7, avoid doing something fancy in ISR, for example Serial.print
// Or you can get this run-time error / crash

