// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 4 Channel OSMC H Bridge Interface

// Duty cycle for the OSMC is 0 to 98% and the frequency is 1kHz

#include "R4_OSMCHBridge.h" 

//https://www.arduino.cc/reference/en/libraries/adafruit-pwm-servo-driver-library/
//Version 2.4.1
Adafruit_PWMServoDriver R4_OSMC         = Adafruit_PWMServoDriver(R4_OSMCHBridge_device_Address);    //Version 2.4.1
Adafruit_PWMServoDriver R4_OSMC_EN_DIS  = Adafruit_PWMServoDriver(R4_OSMCHBridge_EN_device_Address); //Version 2.4.1

osmc_type osmc;

void R4_OSMC_Init()
{
  R4_OSMC.reset();
  R4_OSMC.begin();
  R4_OSMC.setOscillatorFrequency(27000000);
  R4_OSMC.setPWMFreq(1526);                            // This is the PWM frequency 100kHz
  Wire.setClock(400000);
  R4_OSMC_EN_DIS_Init();
}

void R4_OSMC_EN_DIS_Init()
{
  R4_OSMC_EN_DIS.begin();
  R4_OSMC_EN_DIS.setOscillatorFrequency(27000000);
  R4_OSMC_EN_DIS.setPWMFreq(1000);                            // This is the PWM frequency 100kHz
  Wire.setClock(400000);
  R4_OSMC_DisableAll();
}

void R4_OSMC_DisableAll()
{
  R4_OSMC1_Disable();
  R4_OSMC2_Disable();
  R4_OSMC3_Disable();
  R4_OSMC4_Disable();
}

void R4_TestOSMC1()
{
  R4_OSMC1_Enable();
  R4_OSMC1_setSpeedandDirection(500, 0);
}

void R4_TestOSMC2()
{
  R4_OSMC2_Enable();
  R4_OSMC2_setSpeedandDirection(500, 0);
}

void R4_TestOSMC3()
{
  R4_OSMC3_Enable();
  R4_OSMC3_setSpeedandDirection(500, 0);
}

void R4_TestOSMC4()
{
  R4_OSMC4_Enable();
  R4_OSMC4_setSpeedandDirection(500, 0);
}

// ******************************************************************** R4_OSMC1

void R4_OSMC1_Enable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC1_DIS,0,4096);  // ON HIGH use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC1_Disable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC1_DIS,4096,0); // OFF LOW use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC1_setSpeedandDirection(uint16_t speed, bool direction)
{

  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
   osmc.Duration__1 = 4095;
    else if(speed < 1)                                               // make sure this int is not negative!
     osmc.Duration__1 = 1;
      else 
       osmc.Duration__1 = speed;

  osmc.Direction_1 = direction;  
  R4_OSMC_setSpeedandDirection(OSMC1_AHI, OSMC1_ALI, OSMC1_BHI, OSMC1_BLI, osmc.Duration__1, osmc.Direction_1);
}

// ******************************************************************** R4_OSMC2
void R4_OSMC2_Enable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC2_DIS,0,4096);  // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC2_Disable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC2_DIS,4096,0); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC2_setSpeedandDirection(uint16_t speed, bool direction)
{
  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
   osmc.Duration__2 = 4095;
    else if(speed < 1)                                               // make sure this int is not negative!
     osmc.Duration__2 = 1;
      else 
       osmc.Duration__2 = speed;

  osmc.Direction_2 = direction;   
  R4_OSMC_setSpeedandDirection(OSMC2_AHI, OSMC2_ALI, OSMC2_BHI, OSMC2_BLI, osmc.Duration__2, osmc.Direction_2);
}

// ******************************************************************** R4_DHB12_2_A
void R4_OSMC3_Enable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC3_DIS,0,4096);  // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC3_Disable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC3_DIS,4096,0); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC3_setSpeedandDirection(uint16_t speed, bool direction)
{
  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
   osmc.Duration__3 = 4095;
    else if(speed < 1)                                               // make sure this int is not negative!
     osmc.Duration__3 = 1;
      else 
       osmc.Duration__3 = speed;

  osmc.Direction_3 = direction; 
  R4_OSMC_setSpeedandDirection(OSMC3_AHI, OSMC3_ALI, OSMC3_BHI, OSMC3_BLI, osmc.Duration__3, osmc.Direction_3);
}

// **************************************************************************** R4_DHB12_2_B
void R4_OSMC4_Enable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC4_DIS,0,4096);  // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC4_Disable()
{
  R4_OSMC_EN_DIS.setPWM(OSMC4_DIS,4096,0); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_OSMC4_setSpeedandDirection(uint16_t speed, bool direction)
{
  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
   osmc.Duration__4 = 4095;
    else if(speed < 1)                                               // make sure this int is not negative!
     osmc.Duration__4 = 1;
      else 
       osmc.Duration__4 = speed;

  osmc.Direction_4 = direction;   
  R4_OSMC_setSpeedandDirection(OSMC4_AHI, OSMC4_ALI, OSMC4_BHI, OSMC4_BLI, osmc.Duration__4, osmc.Direction_4 );
}

speed_direction_type R4_OSMC_getSpeedandDirection(int channel)
{
    speed_direction_type temp;

    switch (channel) 
    { 
      case 1:
        temp.Direction = osmc.Direction_1;
        temp.Duration  = osmc.Duration__1;
        return temp; 
        break;

      case 2:
        temp.Direction = osmc.Direction_2;
        temp.Duration  = osmc.Duration__2;
        return temp; 
        break;

      case 3:
        temp.Direction = osmc.Direction_3;
        temp.Duration  = osmc.Duration__3;
        return temp; 
        break;

      case 4:
        temp.Direction = osmc.Direction_4;
        temp.Duration  = osmc.Duration__4;
        return temp; 
        break;    

      default:  
        break;
    }
}

void R4_OSMC_setSpeedandDirection(uint8_t R4_OSMC_AHI_channel, uint8_t R4_OSMC_ALI_channel, uint8_t R4_OSMC_BHI_channel, uint8_t R4_OSMC_BLI_channel, uint16_t speed, bool direction)
{
    if (direction) // forwards
    { 
      R4_OSMC.setPWM(R4_OSMC_ALI_channel,0,4096);  //OFF
      R4_OSMC.setPWM(R4_OSMC_AHI_channel,4096,0);  //ON
      R4_OSMC.setPWM(R4_OSMC_BLI_channel,0,speed); //PWM 
      R4_OSMC.setPWM(R4_OSMC_BHI_channel,4096,0);  //ON
    } 
    else           // Reverse
    {
      R4_OSMC.setPWM(R4_OSMC_ALI_channel,0,speed); //PWM
      R4_OSMC.setPWM(R4_OSMC_AHI_channel,4096,0);  //ON
      R4_OSMC.setPWM(R4_OSMC_BLI_channel,0,4096);  //OFF
      R4_OSMC.setPWM(R4_OSMC_BHI_channel,4096,0);  //ON
    }
}

