// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// DHB12 H-Bridge Board controller via PCA96895 PWM IC on the R4 Board
// Duty cycle for the DHB12 is 0 to 98% and the frequency is 1kHz

#include "R4_DHB12HBridge.h"

//https://www.arduino.cc/reference/en/libraries/adafruit-pwm-servo-driver-library/
Adafruit_PWMServoDriver R4_DHB12 = Adafruit_PWMServoDriver(R4_DHB12HBridge_device_Address);

dhb12_type dhb12;
 
void R4_DHB12_Init()
{
  R4_DHB12.reset();
  R4_DHB12.begin();
  R4_DHB12.setOscillatorFrequency(27000000);
  R4_DHB12.setPWMFreq(1526);                            // This is the PWM frequency 100kHz
  Wire.setClock(400000);
}

void R4_TestDHB12()
{
  R4_DHB12_1_A_Enable();
  R4_DHB12_1_A_setSpeedandDirection(1000, 0);
  R4_DHB12_1_B_Enable();
  R4_DHB12_1_B_setSpeedandDirection(1000, 0);
  R4_DHB12_2_A_Enable();
  R4_DHB12_2_A_setSpeedandDirection(1000, 0);
  R4_DHB12_2_B_Enable();
  R4_DHB12_2_B_setSpeedandDirection(1000, 0);
}

void R4_DHB12_DisableAll()
{
  R4_DHB12_1_A_Disable();
  R4_DHB12_1_B_Disable();
  R4_DHB12_2_A_Disable();
  R4_DHB12_2_B_Disable();
}

// ******************************************************************** R4_DHB12_1_A
void R4_DHB12_1_A_Enable()
{
  R4_DHB12.setPWM(DHB12_1_A_EN,4096,0);  // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_1_A_Disable()
{
  R4_DHB12.setPWM(DHB12_1_A_EN,0,4096); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_1_A_setSpeedandDirection(uint16_t speed, bool direction)
{
  // Ensure the duration is within limits.
  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
    speed = 4095;
    else 
     if(speed < 1)                                               // make sure this int is not negative!
      speed = 1;

  dhb12.Duration__1A = speed;
  dhb12.Direction_1A = direction;
  R4_DHB12_setSpeedandDirection(DHB12_1_A_IN1,DHB12_1_A_IN2, speed, direction);
}

// ******************************************************************** R4_DHB12_1_B
void R4_DHB12_1_B_Enable()
{
  R4_DHB12.setPWM(DHB12_1_B_EN,4096,0);  // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_1_B_Disable()
{
  R4_DHB12.setPWM(DHB12_1_B_EN ,0,4096); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_1_B_setSpeedandDirection(uint16_t speed, bool direction)
{
  // Ensure the duration is within limits.
  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
    speed = 4095;
    else 
     if(speed < 1)                                               // make sure this int is not negative!
      speed = 1;

  dhb12.Duration__1B = speed;
  dhb12.Direction_1B = direction;
  R4_DHB12_setSpeedandDirection(DHB12_1_B_IN1,DHB12_1_B_IN2, speed, direction);
}

// ******************************************************************** R4_DHB12_2_A
void R4_DHB12_2_A_Enable()
{
  R4_DHB12.setPWM(DHB12_2_A_EN,4096,0); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_2_A_Disable()
{
  R4_DHB12.setPWM(DHB12_2_A_EN,0,4096); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_2_A_setSpeedandDirection(uint16_t speed, bool direction)
{   
  // Ensure the duration is within limits.
  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
    speed = 4095;
    else 
     if(speed < 1)                                                    // make sure this int is not negative!
      speed = 1;

  dhb12.Duration__2A  = speed;
  dhb12.Direction_2A = direction;
  R4_DHB12_setSpeedandDirection(DHB12_2_A_IN1, DHB12_2_A_IN2, speed, direction);
}

// **************************************************************************** R4_DHB12_2_B
void R4_DHB12_2_B_Enable()
{
    R4_DHB12.setPWM(DHB12_2_B_EN,4096,0); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_2_B_Disable()
{
    R4_DHB12.setPWM(DHB12_2_B_EN,0,4096); // use .setPWM if you want to use PCA9685 channel as a GPIO pin
}

void R4_DHB12_2_B_setSpeedandDirection(uint16_t speed, bool direction)
{
  if (speed > 4095)                                                   //(Yi's code was wrong here - limited the Hi value but not the low value - this code limits the signal between 5% and 10%
    speed = 4095;
    else 
     if(speed < 1)                                               // make sure this int is not negative!
      speed = 1;

  dhb12.Duration__2B  = speed;
  dhb12.Direction_2B = direction;
  R4_DHB12_setSpeedandDirection(DHB12_2_B_IN1, DHB12_2_B_IN2, speed, direction);
}

speed_direction_type R4_DHB12_getSpeedandDirection(int channel)
{
    speed_direction_type temp;

    switch (channel) 
    { 
      case 1:
        temp.Direction = dhb12.Direction_1A;
        temp.Duration  = dhb12.Duration__1A;
        return temp; 
        break;

      case 2:
        temp.Direction = dhb12.Direction_1B;
        temp.Duration  = dhb12.Duration__1B;
        return temp; 
        break;

      case 3:
        temp.Direction = dhb12.Direction_2A;
        temp.Duration  = dhb12.Duration__2A;
        return temp; 
        break;

      case 4:
        temp.Direction = dhb12.Direction_2B;
        temp.Duration  = dhb12.Duration__2B;
        return temp; 
        break;    

      default:  
        break;
    }
}

void R4_DHB12_setSpeedandDirection(uint8_t R4_DHB12IN1_channel, uint8_t R4_DHB12IN2_channel, uint16_t speed, bool direction)
{
    if (direction)
    {                                                                                                       
      // check if boolean 1 is forward or backwards and swap if contents to ensure correct direction 1 = clockwise 0 = anticlockwise 
      R4_DHB12.setPWM(R4_DHB12IN1_channel,0,speed);    
      R4_DHB12.setPWM(R4_DHB12IN2_channel,0,4096);     
    } 
    else 
    {
      R4_DHB12.setPWM(R4_DHB12IN1_channel,0,4096);     
      R4_DHB12.setPWM(R4_DHB12IN2_channel,0,speed); 
    }
}

/*
void brake()
{
    //ledcWrite(Steering_CH_1, 0);
    //ledcWrite(Steering_CH_2, 0); 
}
*/