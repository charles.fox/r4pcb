// Chris Waltham 13-07-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 DMH - this handles the Dead Mans Handle Switch and relay
#include <Arduino.h>
#define DMHRelay_GPIOpin   40

#ifndef R4_DMH_H
  #define R4_DMH_H

  #define AIN4pin    A9   // DMH_IN     J4
  
  void R4_DMH_Init();
  void R4_DMH_Relay_On();
  void R4_DMH_Relay_Off();
  double R4_DMH_ReadVoltage();

#endif