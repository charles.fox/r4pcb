// Chris Waltham 29-06-2023
// cwaltham@lincoln.ac.uk
// https://github.com/Open-Source-R4-Robotics-Platform/R4-Hardware-Master/tree/Working
// University of Lincoln School of Computer Science
// R4 16 Channel Servo Bank Controller via PCA96895 PWM IC on the R4 Board

#include "R4_ServoBank.h"

//https://www.arduino.cc/reference/en/libraries/adafruit-pwm-servo-driver-library/
//Version 2.4.1
Adafruit_PWMServoDriver R4_Servos = Adafruit_PWMServoDriver(R4_ServoBank_device_Address);  //Version 2.4.1

void R4_ServoBank_Init()
{
  R4_Servos.reset();
  R4_Servos.begin();
  R4_Servos.setOscillatorFrequency(27000000);
  R4_Servos.setPWMFreq(50);                            // This is the maximum PWM frequency
  Wire.setClock(400000);
}

void R4_ServoBank_Sleep()
{
  R4_Servos.sleep();
}

void R4_ServoBank_Wakeup()
{
  R4_Servos.wakeup();
}

void R4_ServoBank_test()
{
  // All R4 Servo Channels test
  for (uint16_t i=0; i<4096; i += 8) 
  {
    for (uint8_t pwmnum=0; pwmnum < 16; pwmnum++) 
    {
      R4_Servos.setPWM(pwmnum, 0, (i + (4096/16)*pwmnum) % 4096);
    }
  }
}

void R4_ServoBank_disableAll()
{
 R4_Servos.setPWM(0, 0, 4096);
 R4_Servos.setPWM(1, 0, 4096);
 R4_Servos.setPWM(2, 0, 4096);
 R4_Servos.setPWM(3, 0, 4096);
 R4_Servos.setPWM(4, 0, 4096);
 R4_Servos.setPWM(5, 0, 4096);
 R4_Servos.setPWM(6, 0, 4096);
 R4_Servos.setPWM(7, 0, 4096);
 R4_Servos.setPWM(8, 0, 4096);
 R4_Servos.setPWM(9, 0, 4096);
 R4_Servos.setPWM(10, 0, 4096); 
 R4_Servos.setPWM(11, 0, 4096);
 R4_Servos.setPWM(12, 0, 4096);
 R4_Servos.setPWM(13, 0, 4096);
 R4_Servos.setPWM(14, 0, 4096);
 R4_Servos.setPWM(15, 0, 4096);
}

void R4_ServoBank_setChannel_ServoPulseDuration(uint16_t Microseconds, uint8_t Channel)
{
  R4_Servos.setPWM(Channel,0,Microseconds);
}

uint8_t R4_ServoBank_getChannel_ServoPulseDuration(uint8_t Channel)
{
  return R4_Servos.getPWM(Channel);
}

void R4_ServoBank_setChannel_Ontime_OffTime(uint8_t Channel, uint16_t on_Microseconds, uint16_t off_Microseconds)
{
  R4_Servos.setPWM(Channel, on_Microseconds, off_Microseconds);
}
