# R4 PCB

![image](R4_3DModelImage.png)
 
 The R4 Board with an Arduino Giga R1 provides hardware interface functionality and control code for the following hardware:

 1.  4  x OSMC H Bridges (open source high current Brushed Motor Controller)
 2.  2  x Dual DHB12 H Bridges (currently propriety although later a compatible open Source H Bridge will be engineered)
 3.  4  x Stepper Motor Step and Direction Interface (library is integrated but not available yet)
 4.  4  x Ultrasonic HC-SR04 Sensor Interfaces (library not written yet)
 5.  4  x BLDC Motor Driver Interfaces - 4 Channel DAC and Control signals (library not written yet)
 6.  4  x Quadrature Encode Interfaces (library not written yet)
 7.  6  x Configurable Analog input channels with protection and ranging circuity 50V maximum input voltage.
 8.  16 x Channel RC Servo (PWM) Interface
 9.  8  x Channel 10 Amp Relay Interface
 10. 1  x i2C LCD Interface (library not written yet)
 11. 1  x Provides accesss to an external secondary i2C Interface
 12. 1  x Provides accesss to an external SPI Interface
 13. 2  x UART Interfaces
 14. 1  x Cam Bus interface with CAMBUS Driver
 15. 2  x Accessable LED's
 16. 1  x Accessable Push Button
 17. 1  x 5V onboard Voltage Regulator
 18. 1  x 3.3V onboard Regulator
 19. 1  x External 24V Fused Power input (Automotive Blade Fuse)
 20. 1  x External 12V Fused Power input (Automotive Blade Fuse)
 21. 1  x External  5V Fused Power Input (Automotive Blade Fuse)
 Working
 22. 1  x 'Dead Man's Handle' Saftey Relay and Interface (for E-Stop or similar functionality)
  
 A detailed PCB assembly guide is provided as pdf.

The R4 Board provides access to all Arduino Giga R1 pins (Although most have been allocated functions)
Hardware interfaces are provided via robust connectors rather than header pins in the hope these prove more reliable in use.

The library provides access to the WifiUDP interface (although ROS2Arduino has also been tested), code providing a heartbeat signal and error detection has been written which provides between 100Hz and 200Hz packet rates for sending hardware state information to ROS2. At the same time a heartbeat signal which is expected to be recieved by the R4 and also sent by the R4 to ROS2 detects link losses and re-establishes connection should the Wifi link fail. All datagram packets use a CRC32 packet error detection mechanism to detect errors which may lead to erratic behavoir if left undetected.

A ROS2 to R4 communications node is also in development (see the other repo's on this gitHub for details), this currently provides sufficient functionality to connect the R4 to ROS2 and publishes hardware state information for ROS2 subscriber nodes, timing packets as it goes. All datagram packet transfers from the R4 to ROS2 are sent at specific time intervals. A hardware timer and ISR are used to ensure timley delivery of state information to ROS2, the heartbeat signal is also sent once per second controlled by a timer.

Asyncrounous 'Command Packets' are sent to R4 so that ROS2 is able to request a change to hardware parameters. The intent here is to ACK each 'Command Packet' so that the ROS2 node has explicit confirmation that the 'Command Packet' was received.

A high datagram frequency allows closed loop control of hardware to be moved from the microcontroller to ROS2 nodes allowing greater flexibilty in functionality without re-writing microcontroller code. ROS2 nodes can be implemeted for whichever control architecture is requried for the application.

R4 is intended to provide an open source hardware platform which is suitable for many robotics applications or motion system.
Wheeled robots with 2 or 4 wheel steering, BLDC, Brushed or Servo motors with or without encoder feedback are possible with the R4 board.

A portenta H7 timer library is used and requires a minor change ( change library TimerInterrupt.h by commentling lines 30-47 - Default location for Windows is Documents/Arduino). The specific libraries used and their compatible versions are as listed below:

1. Portenta_H7_TimerInterrupt - v1.4.0 
2. Adafruit PWM ServoDriver Library - v2.4.1 (install with dependencies)
3. DFRobot_MCP23017 - v1.0.0

The code here can be compiled using both the Arduino IDE (v2) and Visual Studio Code.

![image](R4_3DModelImage2.png)

If you use R4 in your work, please cite it as 

* Kshitij Gaikwad, Rakshit Soni, Charles Fox and Chris Waltham.  Open source hardware robotics interfacing board.   Proceedings of Towards Autonomous Robots (TAROS), 2023.



# Software dependencies
                                                                                                   
RPC                               1.0                
rpclib                            1.0.0           
openamp                           1.0     
Wire                                               
AccelStepper                      1.64                                                      
Adafruit PWM Servo Driver Library 2.4.1                                
Portenta_H7_TimerInterrupt        1.4.0                                       
DFRobot_MCP23017                  1.0.0                                                 
WiFi                              1.0           
SocketWrapper                     1.0      
Arduino_CRC32                     1.0.0
=======

