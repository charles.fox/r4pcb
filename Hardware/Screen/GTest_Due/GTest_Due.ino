//#define F_CPU 1000000L
//STM32H747X

#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0
#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

//#include <SPI.h>          // f.k. for Arduino-1.5.2
#include "Adafruit_GFX.h"// Hardware-specific library
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

String inputString = "";         // a String to hold incoming data
String inputString1 = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete
bool stringComplete1 = false;  // whether the string is complete

void setup(void) 

{
    Serial.begin(9600);
    inputString.reserve(100);
    Serial1.begin(9600);
    inputString1.reserve(100);
    
    uint32_t when = millis();
    Serial.println("Serial took " + String((millis() - when)) + "ms to start");
    uint16_t ID = tft.readID(); //
    Serial.print("TFT ID = 0x");
    Serial.println(ID, HEX);
    delay(2000);
    
    if (ID == 0xD3D3) ID = 0x9481; // write-only shield
    tft.begin(ID);
    tft.setRotation(3);
    tft.fillScreen(BLACK);
    tft.setTextColor(WHITE);
    tft.setCursor(0, 0);
    tft.setTextSize(2);
}

void loop(void) 
{  
    tft.setCursor(0, 0);
    tft.print("R4 DEBUG"); 
    delay(100);
}

void serialEvent() 
{
  while(Serial.available()) 
  {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') 
    {
      Serial1.println(inputString);
      Serial.println(inputString); //Echo
      tft.setCursor(0, 16);
      tft.print(inputString); 
      inputString = "";
    }
   }
}

void serialEvent1() 
{
  while (Serial1.available()) 
  {
    // get the new byte:
    char inChar1 = (char)Serial1.read();
    // add it to the inputString:
    inputString1 += inChar1;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar1 == '\n') 
    {
      Serial.println(inputString);
      tft.setCursor(0, 32);
      tft.print(inputString1); 
      inputString1 = "";
    }
  }
}
